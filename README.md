# Project 4: Brevet time calculator with Ajax
# Author: Otis Greer
# Email: ogreer@uoregon.edu
# Description: What this program does is takes in the values for a given brevet distance, starting date, and starting time. After these are set, then the user can type in values for the controle distances within the brevet in either miles or kilometers. Then the program will calculate the time for opening the controle gate and closing the controle gate based off of the speeds and distances specified on the website "https://rusa.org/pages/acp-brevet-control-times-calculator"  
